#!/bin/bash

# Development test - test all make targets

set -eux
make install-all
make install-hermit
make install
make fix-format
make all
make build

make builder
make all-docker
CMD=ls make builder-shell
make lint
make test
