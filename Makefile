GO=$(shell which go)
MEGA_LINTER_VERSION=v4.46.0
MODULE=$(shell head -n 1 go.mod | cut -d ' ' -f 2)

.PHONY: all
all:
	make install
	make test
	make build

.PHONY: all-docker
all-docker:
	docker run -it -v ${PWD}:/work ${MODULE}/builder make all

.PHONY: build
build:
	go build .
	yarn workspace webapp build

.PHONY: builder
builder:
	docker build -f builder.Dockerfile -t ${MODULE}/builder .

.PHONY: builder-shell
builder-shell:
	docker run -it -t ${MODULE}/builder ${CMD}

.PHONY: clean
clean:
	rm -rf node_modules packages/webapp/node_modules packages/webapp/.next
	rm -f boilerplate
	eval "sudo ${GO} clean -cache -modcache -i -r"
	sudo rm -rf report

.PHONY: dev
dev:
	yarn workspace webapp dev

.PHONY: format
fix-format:
	gofmt -l -w .

.PHONY: install
install:
	npm install --global yarn
	yarn install
	touch node_modules/go.mod

.PHONY: install-all
install-all: install-hermit install

.PHONT: install-hermit
install-hermit:
	hermit install
	. ./bin/activate-hermit
	hermit shell-hooks --bash

.PHONY: lint
lint:
	docker run -it -v ${PWD}:/tmp/lint nvuillam/mega-linter:${MEGA_LINTER_VERSION}

.PHONY: run
run: build
	./boilerplate

.PHONY: test
test:
	go test -tags test -v -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -html=profile.cov -o profile.html
	go tool cover -func profile.cov
	go vet ./...
  # validate formatting (use make format to fix)
	[ "`gofmt -l -s .`" = "" ]
  # open coverage
	go tool cover -html=profile.cov
