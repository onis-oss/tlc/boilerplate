//go:build !test

package main

import (
	"log"
	"os"

	"gitlab.com/onis-oss/public/boilerplate/cmd"
)

func main() {
	app := cmd.NewApp()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
