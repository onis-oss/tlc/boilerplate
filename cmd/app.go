package cmd

import (
	"fmt"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
	slog "log"
	"os"
)

func init() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	slog.SetFlags(0)
	slog.SetOutput(log.Logger)
}

func NewApp() *cli.App {
	app := &cli.App{
		Before: func(c *cli.Context) error {
			switch {
			case c.Bool("debug"):
				zerolog.SetGlobalLevel(zerolog.DebugLevel)
			case c.Bool("quiet"):
				zerolog.SetGlobalLevel(zerolog.ErrorLevel)
			}
			return nil
		},
		Commands: []*cli.Command{
			runCmd,
		},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "debug",
				Aliases: []string{"d"},
			},
			&cli.BoolFlag{
				Name:    "quiet",
				Aliases: []string{"q"},
			},
		},
		Name:    "boilerplate",
		Usage:   "my boilerplate app",
		Version: "v1.0.0",
	}
	return app
}

var runCmd = &cli.Command{
	Action: func(c *cli.Context) error {
		fmt.Println("Hello boilerplate!")
		log.Info().Msg("Boilerplate info log")
		log.Debug().Msg("Boilerplate debug log")
		return nil
	},
	Name:  "run",
	Usage: "run my app",
}
