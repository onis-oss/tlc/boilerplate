package cmd_test

import (
	"github.com/stretchr/testify/assert"
	"testing"

	"gitlab.com/onis-oss/public/boilerplate/cmd"
)

func TestRoot(t *testing.T) {
	app := cmd.NewApp()
	err := app.Run([]string{"boilerplate", "run"})
	assert.NoError(t, err)
	err = app.Run([]string{"boilerplate", "-d", "run"})
	assert.NoError(t, err)
	err = app.Run([]string{"boilerplate", "-q", "run"})
	assert.NoError(t, err)
}
